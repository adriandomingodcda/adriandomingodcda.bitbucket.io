(function ($) {
  $(document).ready(function() {
    // Information
    $('.map img').click(function(e) {
      var offset = $(this).offset();
      var X = (e.pageX - offset.left);
      var Y = (e.pageY - offset.top);
      $('.output').text('X: ' + X + ', Y: ' + Y);
    });

    var map = $('.map');
    var clickable_diameter = 20;

    // Array of people
    var people = [
      {
        name:'Adrian Domingo',
        info:'Senior Developer',
        photo:'male',
        coords: {x:164, y:412},
      },
      {
        name:'Greg Bailey',
        info:'Artworker',
        photo:'male',
        coords: {x:164, y:359},
      },
      {
        name:'Gavin Brown',
        info:'Senior Designer',
        photo:'male',
        coords: {x:108, y:359},
      },
      {
        name:'Michael Simpson',
        info:'Senior Artworker',
        photo:'male',
        coords: {x:38, y:412},
      },
      {
        name:'Spencer Bliss',
        info:'Design Director',
        photo:'male',
        coords: {x:38, y:359},
      },
    ];

    createClickable = function (person) {
      coords = person['coords'];

      popup_info = `
<div class="person-popup">
  <div class="person-popup-contents">
    <div class="photo"><img src="images/${person['photo']}.svg"></div>
    <div class="details">
      <div class="name">${person['name']}</div>
      <div class="info">${person['info']}</div>
    </div>
  </div>
</div>`;

      click_region = `
<div class="click-region"></div>`;

      offset_x = coords.x-(clickable_diameter/2);
      offset_y = coords.y-(clickable_diameter/2);

      clickable_area = `<div class="click-area" style="left:${offset_x}px; top:${offset_y}px">${click_region}${popup_info}</div>`;
      return clickable_area;
    };

    // Generate clickable areas.
    for (var i = people.length - 1; i >= 0; i--) {
      var clickable_item = createClickable(people[i]);
      map.append(clickable_item);
    }

    // Click functionality
    $('.click-region').click(function(event) {
      event.preventDefault();
      // Close other popups
      $('.person-popup').hide();
      // Open this popup.
      $(this).siblings('.person-popup').show();
    });

    // Hide all on clickout.
    $('.map img').click(function(event) {
      $('.person-popup').hide();
    });

  });
})(jQuery);
